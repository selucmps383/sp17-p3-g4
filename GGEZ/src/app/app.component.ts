import { Component } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor (private http: Http) {}
  // private instance variable to hold base url
  //private commentsUrl = ;

  summonerName = '';

  search(value: string){
    this.summonerName = value;
    let searchUrl = 'https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/' + value + '?api_key=RGAPI-12782adf-872a-46ec-abae-3db6f79cdf63';
    console.log('searching');
    return this.http.get(searchUrl)
      .map((res:Response) => {
        res.json();

      })
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  onEnter(value: string){
    console.log(value);
    this.search(value)
      .subscribe(
      data => console.log(data),
      error => console.log(error))
  }
}
