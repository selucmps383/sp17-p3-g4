/**/
export interface MatchList{

//Summoner ID
summonerId: string

//MatchList
endIndex:	number
matches:	number[]	
startIndex:	number	
totalGames:	number

//Match Referance
champion:   number	
lane:	string	
matchId: number	
platformId:	string	
queue:	string	
region:	string	
role:	string	
season:	string
timestamp:	number


}